#ifndef GYROSCOPE_H
#define GYROSCOPE_H

struct Gyroscope
{
    uint8_t id; // ID of the sensor
    float x;    // Get rotational velocity in deg/s, typical longitudinally
    float y;    // Get rotational velocity in deg/s, typical laterally
    float z;    // Get rotational velocity in deg/s, typical vertically

    Gyroscope( ) :
        id(0),
        x(0),
        y(0),
        z(0)
    {
    }
};

#endif /* GYROSCOPE_H */
