#ifndef GPS_H
#define GPS_H

struct GPS
{
private:
    uint16_t groundSpeed;        // ground speed of the vehicle, in km/hr * 10, USE GETTERS and SETTERS

public:
    float latitude;
    float longitude;
    uint8_t satellites;
    bool locked;

    GPS( ) :
        groundSpeed(0),
        latitude(0),
        longitude(0),
        satellites(0),
        locked(false)
    {

    }

    float GetGroundSpeed() const
    {
        return (float)(groundSpeed) / 10.0;
    }

    void SetGroundSpeed(const float speed)
    {
        groundSpeed = (uint16_t)(std::abs(speed)*10);
        return;
    }
};


#endif /* GPS */
