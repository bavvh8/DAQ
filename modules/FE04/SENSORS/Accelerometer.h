#ifndef ACCELEROMETER_H
#define ACCELEROMETER_H


struct Accelerometer
{
    uint8_t id; // ID of the sensor
    float x;    // Get acceleration in m/s/s, typical longitudinally
    float y;    // Get acceleration in m/s/s, typical laterally
    float z;    // Get acceleration in m/s/s, typical vertically

    Accelerometer( ) :
        id( 0 ),
        x(0),
        y(0),
        z(0)
    {
    }
};

#endif /* ACCELEROMETER_H */
