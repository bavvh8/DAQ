//based on Accelerometer.h

#ifndef HALL_EFFECT_H
#define HALL_EFFECT_H


struct HallEffect
{
    uint8_t id; // ID of the sensor
    float rpm;    //will probably add more later

    HallEffect():
        id( 0 ),
        rpm(0)
    {
    }
};


#endif /* HALL_EFFECT_H */
