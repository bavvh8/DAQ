#ifndef CORNER_BOARD_H
#define CORNER_BOARD_H

#include "modules/CAN/bus.h"
#include "modules/FE04/SENSORS/HallEffect.h"
#include "modules/FE04/SENSORS/Infrared.h"
#include "modules/FE04/SENSORS/LinearPotentiometer.h"

struct CornerBoard
{
    uint32_t baseSendingID;
    uint32_t baseReceptionID;
    uint32_t lastTalk;

    HallEffect hallEffect;
    Infrared ir;
    LinearPotentiometer linearPot;

    CornerBoard();
    CornerBoard( const uint32_t baseReceptionID );

    bool ParseFrame( const CANFrame & frame, const uint32_t millis );// need to make!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
};

#endif /* CORNER_BOARD_H */
