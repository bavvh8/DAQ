#ifndef IMU_BOARD_H
#define IMU_BOARD_H

#include "modules/CAN/bus.h"
#include "modules/FE04/SENSORS/GPS.h"
#include "modules/FE04/SENSORS/Gyroscope.h"
#include "modules/FE04/SENSORS/Accelerometer.h"

struct ImuBoard
{
    uint32_t baseSendingID;
    uint32_t baseReceptionID;
    uint32_t lastTalk;

    Accelerometer accel;
    Gyroscope gyro;
    GPS gps;

    ImuBoard();
    ImuBoard( const uint32_t baseReceptionID );

    bool ParseFrame( const CANFrame & frame, const uint32_t millis );// need to make!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
};

#endif /* IMU_BOARD_H */
