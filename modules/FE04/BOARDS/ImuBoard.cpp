#include "ImuBoard.h"

ImuBoard::ImuBoard() :
    baseReceptionID(0),
    lastTalk(0)
{

}

ImuBoard::ImuBoard( const uint32_t baseReceptionID ) :
    baseReceptionID(baseReceptionID),
    lastTalk(0)
{

}

bool ImuBoard::ParseFrame( const CANFrame & frame, const uint32_t millis )
{
    bool ret = false;

    if( frame.ID == baseReceptionID )
    {
        // GPS Data
        uint16_t buffer = frame.GetDataByte( 1 );
        buffer <<= 8;
        buffer |= frame.GetDataByte( 0 );

        gps.SetGroundSpeed(buffer/10.0);
        gps.satellites = frame.GetDataByte( 2 );
        gps.locked = frame.GetDataByte( 7 );

        ret = true;
    }
    else if( frame.ID == baseReceptionID+1 )
    {
        // GPS Position
        union
        {
            float f;
            uint32_t i;
        } retBuffer;

        retBuffer.i =  ( (uint32_t)(frame.GetDataByte(0)) << 24 ) & 0xFF000000;
        retBuffer.i |= ( (uint32_t)(frame.GetDataByte(1)) << 16 ) & 0x00FF0000;
        retBuffer.i |= ( (uint32_t)(frame.GetDataByte(2)) << 8  ) & 0x0000FF00;
        retBuffer.i |= ( (uint32_t)(frame.GetDataByte(3)) << 0  ) & 0x000000FF;

        gps.latitude = retBuffer.f;
        retBuffer.i = 0;

        retBuffer.i =  ( (uint32_t)(frame.GetDataByte(4)) << 24 ) & 0xFF000000;
        retBuffer.i |= ( (uint32_t)(frame.GetDataByte(5)) << 16 ) & 0x00FF0000;
        retBuffer.i |= ( (uint32_t)(frame.GetDataByte(6)) << 8  ) & 0x0000FF00;
        retBuffer.i |= ( (uint32_t)(frame.GetDataByte(7)) << 0  ) & 0x000000FF;

        gps.longitude = retBuffer.f;

        ret = true;
    }
    else if( frame.ID == baseReceptionID+2 )
    {
        // Accel Position
        uint32_t buffer = 0;
        uint32_t signBuffer = 0xFFF80000;

        buffer = frame.GetDataByte(1);
        buffer <<= 8;
        buffer |= frame.GetDataByte(0);
        buffer |= ((uint32_t)(frame.GetDataByte(6)) & 0x7) << 16;
        signBuffer = (frame.GetDataByte(6) & 0x8 ? signBuffer : ~signBuffer);
        signBuffer &= 0xFFF80000;
        buffer |= signBuffer;

        accel.x = (float)((int32_t)(buffer)) / 1000.0;

        signBuffer = 0xFFF80000;
        buffer = frame.GetDataByte(3);
        buffer <<= 8;
        buffer |= frame.GetDataByte(2);
        buffer |= ((uint32_t)(frame.GetDataByte(6)) & 0x70) << 12;
        signBuffer = (frame.GetDataByte(6) & 0x80 ? signBuffer : ~signBuffer);
        signBuffer &= 0xFFF80000;
        buffer |= signBuffer;

        accel.y = (float)((int32_t)(buffer)) / 1000.0;

        signBuffer = 0xFFF80000;
        buffer = frame.GetDataByte(5);
        buffer <<= 8;
        buffer |= frame.GetDataByte(4);
        buffer |= ((uint32_t)(frame.GetDataByte(7)) & 0x7) << 16;
        signBuffer = (frame.GetDataByte(7) & 0x8 ? signBuffer : ~signBuffer);
        signBuffer &= 0xFFF80000;
        buffer |= signBuffer;

        accel.z = (float)((int32_t)(buffer)) / 1000.0;

        accel.id = (frame.GetDataByte(7) >> 4) & 0xF;

        ret = true;
    }
    else if( frame.ID == baseReceptionID+3 )
    {
        // Gyro Position
        uint16_t buffer = 0;

        // X
        buffer = frame.GetDataByte(0);
        buffer <<= 8;
        buffer |= frame.GetDataByte(1);
        gyro.x = (int16_t)(buffer) / 10.0;

        // Y
        buffer = frame.GetDataByte(2);
        buffer <<= 8;
        buffer |= frame.GetDataByte(3);
        gyro.y = (int16_t)(buffer) / 10.0;

        // Z
        buffer = frame.GetDataByte(4);
        buffer <<= 8;
        buffer |= frame.GetDataByte(5);
        gyro.z = (int16_t)(buffer) / 10.0;

        gyro.id = frame.GetDataByte(7);

        ret = true;
    }
    else if( frame.ID == baseReceptionID+4 )
    {
        // Environment


        ret = true;
    }

    if( ret )
    {
        lastTalk = millis;
    }

    return ret;
}
