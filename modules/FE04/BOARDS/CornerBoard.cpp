#include "CornerBoard.h"

CornerBoard::CornerBoard() :
    baseReceptionID(0),
    lastTalk(0)
{

}

CornerBoard::CornerBoard( const uint32_t baseReceptionID ) :
    baseReceptionID(baseReceptionID),
    lastTalk(0)
{

}

bool CornerBoard::ParseFrame( const CANFrame & frame, const uint32_t millis )
{
    bool ret = false;

    if( frame.ID == baseReceptionID )
    {
        // HallEffect data
        //what goes here?????????????????????????????????????????????????????
        ret = true;
    }
    else if( frame.ID == baseReceptionID+1 )
    {
        // IR data

        ret = true;
    }
    else if( frame.ID == baseReceptionID+2 )
    {
        // linearPot data

        ret = true;
    }

    else if( frame.ID == baseReceptionID+3 )
    {
        // Environment


        ret = true;
    }

    if( ret )
    {
        lastTalk = millis;
    }

    return ret;
}
