################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../modules/CAN/can_tests/can_frame_tests.cpp \
../modules/CAN/can_tests/can_identifier_tests.cpp \
../modules/CAN/can_tests/can_simulation_interface_tests.cpp 

OBJS += \
./modules/CAN/can_tests/can_frame_tests.obj \
./modules/CAN/can_tests/can_identifier_tests.obj \
./modules/CAN/can_tests/can_simulation_interface_tests.obj 

CPP_DEPS += \
./modules/CAN/can_tests/can_frame_tests.d \
./modules/CAN/can_tests/can_identifier_tests.d \
./modules/CAN/can_tests/can_simulation_interface_tests.d 

OBJS__QUOTED += \
"modules\CAN\can_tests\can_frame_tests.obj" \
"modules\CAN\can_tests\can_identifier_tests.obj" \
"modules\CAN\can_tests\can_simulation_interface_tests.obj" 

CPP_DEPS__QUOTED += \
"modules\CAN\can_tests\can_frame_tests.d" \
"modules\CAN\can_tests\can_identifier_tests.d" \
"modules\CAN\can_tests\can_simulation_interface_tests.d" 

CPP_SRCS__QUOTED += \
"../modules/CAN/can_tests/can_frame_tests.cpp" \
"../modules/CAN/can_tests/can_identifier_tests.cpp" \
"../modules/CAN/can_tests/can_simulation_interface_tests.cpp" 


